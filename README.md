# Hazapi Web
A web page to generate pizza recipes. Deployed at [Netlify](https://pizza.libreinside.com)

## Pre-requisites
- nodejs v16.16.0+
- npm 8.11.0+

## Quick start

Install dependencies:
```shell
npm install
```

Run it (dev mode):
```shell
npm run dev
```

## Preview before push
Build it:
```shell
npm run build
```

Run in preview:
```shell
npm run preview
```
